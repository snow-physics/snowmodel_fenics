import fenics
#===============================================================================
# # Constants, Parameters
#===============================================================================
# some numerical values (om: order of magnitude), all units in mks

#snow
density_snow_om = 300.0                                     # [kg/m^3]
density_ice_om  = 917.0                                     # [kg/m^3]
ice_volume_fraction_om = density_snow_om / density_ice_om   # [-]
temperature_om = 273.0                                      # [K]
ssa_v_om = 3770                                             # [m^-1]
beta_om = 5.5e5                                             # [s/m]
alpha_om = 0.0
rhoCp_om = 5.5e5                                           # [J/m^3/K]
latent_heat_sublimation = 2.6e9                             # [J/kg]
density_air_om = 1.34                                       # [kg/m^3]
specific_heat_air_om = 1005.0                                 # [J/kg/K]
specific_heat_ice_om = 2000.0                                # [J/kg/K]
ice_air_surface = 3770.0                                      # [m^-1]

#===============================================================================
# # Parametrizations for the PDE coefficients
#===============================================================================

def equilibrium_vapor_density( temperature ):
    """equilibrium vapor density, temperature in K (from Libbrecht)"""
    T_celsius = temperature - 273.15
    peq = ((3.6636e12 -1.3086e8 * T_celsius -3.3793e6 * T_celsius**2) * fenics.exp(-6150.0/temperature)) # pressure in mbar
    return peq / 461.31 / temperature               # concentration via ideal gas

def equilibrium_vapor_density_lin( temperature ):
    T_min = 250
    T_max = 273
    rhoveq_min = 6.5838e-04
    rhoveq_max = 0.0047885
    return rhoveq_min + (rhoveq_max - rhoveq_min)/(T_max - T_min) * (temperature - T_min)

def equilibrium_vapor_density_poly( temperature ):
    T_0 = 245
    T_1 = 255
    T_2 = 273
    c = 4.0662e-04
    rhoveq_min_1 = 0.0010456
    rhoveq_min_2 = 0.0047885
    a = ( (rhoveq_min_1 - c)/(T_1 - T_0) - (rhoveq_min_2 - c)/(T_2 - T_0) ) / (T_1 - T_2)
    b = ( rhoveq_min_1 - c - a*(T_1-T_0)**2 )/(T_1-T_0)
    return a*(temperature-T_0)**2 + b*(temperature-T_0) + c


def effective_thermal_conductivity (ice_volume_fraction):
    """empirical fit (from Calonne)"""
    temp =  0.024 -1.23e-4*(density_ice_om*ice_volume_fraction) + 2.5e-6*(density_ice_om*ice_volume_fraction)**2
    return 1 * temp

def effective_diffusion_constant(ice_volume_fraction):
    """self consistent estimate for spheres (from Calonne)"""
    D_0 = 2.04e-5
    temp =  D_0 * (1 - 3/2*ice_volume_fraction)*fenics.conditional(fenics.ge(2.0/3.0-ice_volume_fraction,0.0),1.0,0.0)
    return 1 * temp

def rhoCp_eff(ice_volume_fraction):
    return ice_volume_fraction*density_ice_om*specific_heat_ice_om + (1-ice_volume_fraction)*density_air_om*specific_heat_air_om

def svn(T,rho_v):
    rho_v_eq = equilibrium_vapor_density(T)
    return ice_air_surface/beta_om * (rho_v-rho_v_eq)/rho_v_eq

def vn(T,rho_v):
    rho_v_eq = equilibrium_vapor_density(T)
    return 1/beta_om * (rho_v-rho_v_eq)/rho_v_eq

def vn_func(T,rho_v):
    rho_v_eq = equilibrium_vapor_density(T)
    return 1/beta_om * (rho_v-rho_v_eq)/rho_v_eq

def d_rho_sat_dT(T):
    sc = 461.31
    a0 = 3.6636e12
    a1 = -1.3086e8
    a2 = -3.3793e6
    Tref = 6150.0
    Tc = T-273.15
    return 1/sc*( (a1+2*a2*Tc)/T*fenics.exp(-Tref/T) - (a0+a1*Tc+a2*Tc**2)/(T**2)*fenics.exp(-Tref/T) + (a0+a1*Tc +a2*Tc**2)*(Tref)/(T**3)*fenics.exp(-Tref/T) )

def Deff_H(phi,T):
    Dref = 0.26e-4
    Tref = 298.0
    Dva = Dref*(T/Tref)**(1.5)
    tc = T-273.15
    k_ice = 1.16*(1.91-8.66e-3*tc+2.97e-5*tc**2)
    k_air = 0.0223 + 8.e-5*(T-250.0)
    dg_dte = d_rho_sat_dT(T)

    return phi*(1-phi)*Dva + (1-phi)*(k_ice*Dva)/(phi*(k_air+latent_heat_sublimation*Dva*dg_dte)+(1-phi)*k_ice)

def keff_H(phi,T):
    Dref = 0.26e-4
    Tref = 298.0
    Dva = Dref*(T/Tref)**(1.5)
    tc = T-273.15
    k_ice = 1.16*(1.91-8.66e-3*tc+2.97e-5*tc**2)
    k_air = 0.0223 + 8.e-5*(T-250.0)
    dg_dte = d_rho_sat_dT(T)

    k_pore = phi*k_ice + (1-phi)*k_air
    k_lam = k_ice*k_air/(phi*(k_air+latent_heat_sublimation*Dva*dg_dte)+(1-phi)*k_ice)
    return phi*k_pore + (1-phi)*k_lam


#===============================================================================
# temperature equation
kappa = effective_thermal_conductivity (ice_volume_fraction_om)/rhoCp_om
