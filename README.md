# snowmodel_fenics

This python code uses the Finite Element library FENICS (via docker)
to solve the one dimensional partial differential equations for heat
and mass transfer in snow. The results are written in vtk format from
which the paper figure is reproduced.


# to just reproduce the figure
- run <plot_meshres_timestep.py> to recreate the figure 


# to reproduce the simulation and the figure:
- run <bash startdocker.sh> to start the fenics docker environment
- navigate to the current directory <cd shared>
- start the simulation over different mesh resolutions with <python3 calonne_ice_vapor_heat.py>
- the simulation with small timestep (dt=2) may take several hours to complete
- to plot the results, enter an environment with the vtk library installed
- run <plot_meshres_timestep.py> to recreate the figure 