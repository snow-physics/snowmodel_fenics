import fenics

def define_boundaries(H):
    # define top boundary
    class top(fenics.SubDomain):
        def inside(self, x, on_boundary):
            return on_boundary and fenics.near(x[0], H)

    #define boundary bottom side
    class bottom(fenics.SubDomain):
        def inside(self, x, on_boundary):
            return on_boundary and fenics.near(x[0], 0.0)

    top = top()
    bottom = bottom()

    return top, bottom
