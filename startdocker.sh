#!/bin/bash
# run docker container with fenics 2017.2.0

# stop running existing container
docker stop fenics-2017.2.0

# remove existing container
docker rm fenics-2017.2.0

# start new container
#docker run -ti -v "$(pwd):/home/fenics/shared" --name "fenics-2017.2.0" quay.io/fenicsproject/stable:2017.2.0
docker run -ti -v $(pwd):/home/fenics/shared --privileged=true --env HOST_UID=$(id -u) --env HOST_GID=$(id -g) --name fenics-2017.2.0 quay.io/fenicsproject/stable:2017.2.0

bash -c
