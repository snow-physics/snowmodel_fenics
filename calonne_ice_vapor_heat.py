# coding: utf-8

import fenics
import matplotlib.pyplot as plt
import time as cputime
import numpy as np

from pathlib import Path

import math
# import tqdm

# determine fenics output
fenics.set_log_level(30)

#===============================================================================
# # Constants, Parameters
#===============================================================================
from parameters import *
from def_boundary import *


class CalonneIceVaporHeat():
    """
    fenics object to setup and solve coupled calonne vapor, heat and v_n equations with decoupled ice equation
    """
    def __init__(self,ne=1000,dt=60, verbosity=0):
        super(CalonneIceVaporHeat,self).__init__()
        self.ne = int(ne)
        self.dt = int(dt)
        self.verbosity=verbosity
        # setup domain
        print("setup domain")
        self.setup_domain()
        # setup mesh
        print('setup mesh')
        self.setup_mesh()
        # setup function spaces for FE approximation
        print('setup function space')
        self.setup_function_space()
        print('import & apply boundary conditions')
        # import and apply boundary conditions
        self.set_boundary_conditions()
        # import and apply initial conditions
        print('import & apply initial conditions')
        self.set_initial_conditions()
        # define variational problem
        print('setup variational problem')
        self.setup_variational_problem()
        # setup solver
        print('setup solver settings')
        self.setup_solver()

    #===============================================================================
    # # Domain setup
    #===============================================================================
    def setup_domain(self):
        """
        set up time and space domain paramters
        """
        # time
        self.t_end  = 60.0*60*144        # end time in seconds (144 hours)
        # self.dt     = 60.0               # time discretization time step in seconds
        self.dt_out = 600.0              # output timestep
        # space
        self.snow_depth = 0.02           # z=axis / height of the snowpack
        self.H_p        = 1.0            # normalization factor for discretization. If set to self.snow_depth, the domain is normalized to 1.0 for the solution. May stabilize results.
        # normalized snow depth
        self.H          = self.snow_depth/self.H_p

    #===============================================================================
    # # Mesh #######################################################################
    #===============================================================================
    def setup_mesh(self):
        """
        setup mesh for FE discretization
        """
        # number of elements of FE mesh
        # initialize equidistant mesh with "ne" elements from range 0.0 to self.H
        self.mesh = fenics.IntervalMesh(self.ne, 0.0, self.H)
        
    #===============================================================================
    # # Define function spaces, basis and test functions
    #===============================================================================
    def setup_function_space(self):
        """
        FE approximation requires function spaces. 
        The function spaces define how to interpolate the solution between the nodal values (linear, polynomial, etc.) and so implicitly define how many nodal values per element exist.  
        https://fenicsproject.org/olddocs/dolfin/1.3.0/python/programmers-reference/functions/functionspace/FunctionSpace.html
        """
        ## DEFINE FUNCTION SPACE ###


        # To define the function spaces, we begin with defining the element types.        
        P1 = fenics.FiniteElement('CG', self.mesh.ufl_cell(),1)
        P2 = fenics.FiniteElement('CG', self.mesh.ufl_cell(),1)
        P3 = fenics.FiniteElement('CG', self.mesh.ufl_cell(),1)
        P4 = fenics.FiniteElement('CG', self.mesh.ufl_cell(),1)
        
        # function space for phi_ice, which is separate (continuous galerkin)
        self.W_phi = fenics.FunctionSpace(self.mesh,P1)
        # rho eq get's solved for on the side and get's an extra function space
        self.W_rho_eq = fenics.FunctionSpace(self.mesh,P2)

        ## function space for vn, rho and T are solved for in a coupled pde problem. They therefore need ot be defined on a common function space        
        mixed_element = fenics.MixedElement([P2,P3,P4])
        self.W_mixed = fenics.FunctionSpace(self.mesh,mixed_element)
        
        # retrieve individual function spaces out 
        self.W_vn = self.W_mixed.sub(0)
        self.W_r = self.W_mixed.sub(1)
        self.W_T = self.W_mixed.sub(2)

        ## DEFINE TRIAL AND TEST FUNCTIONS 
        # on the function spaces the trial and test functions for all variables are defined
        # get trial functions
        self.phi_i = fenics.TrialFunction(self.W_phi)
        self.vn, self.rho, self.T = fenics.TrialFunctions(self.W_mixed)
        self.rho_eq = fenics.TrialFunction(self.W_rho_eq)

        # get the test functions
        self.psi_phi = fenics.TestFunction(self.W_phi)
        self.psi_vn, self.psi_rho, self.psi_T = fenics.TestFunctions(self.W_mixed)
        self.psi_rho_eq = fenics.TestFunction(self.W_rho_eq)
        
    #===============================================================================
    # # Boundary conditions ########################################################
    #===============================================================================
    def set_boundary_conditions(self,):
        """
        defines boundary_condition_expressions. These have to follow a specific format and are included in the variational problem later.
        # boundary conditions may be time-dependent, then .time needs to be updated at solution time.
        """
        # set boundary values
        T_top_0_p       = temperature_om    # base top temperature in K
        T_bottom_0_p    = temperature_om    # base bottom temperature in K
        dTemp_p         = 20.0              # temperature drop at top in K
        t_tau           = 60.0*60*5         # time for temperature drop to manifest in s

        # define boundaries
        top, bottom = define_boundaries(self.H)

        # define classes with boundary conditions for T and rho. T drops linearly, rho is the eq. vapor density for the T. Not used in this simluation
        class TBoundaryExpression(fenics.Expression):
            def __init__(self,Tt0,dT,time,tau_ext, **kwargs):
                self.Tt0            = Tt0
                self.dT             = dT
                self.time           = time
                self.tau_ext        = tau_ext
            def eval(self, value, x):
                T = self.Tt0 - self.dT * (self.time*1.0/self.tau_ext * fenics.conditional(fenics.ge(self.time,0.0),1.0,0.0) - (self.time - self.tau_ext)*1.0/self.tau_ext * fenics.conditional(fenics.ge(self.time-self.tau_ext,0.0),1.0,0.0)  )
                value[0] = T

        class rhoBoundaryExpression(fenics.Expression):
            def __init__(self,Tt0,dT,time,tau_ext, rho_scaling, **kwargs):
                self.Tt0            = Tt0
                self.dT             = dT
                self.time           = time
                self.tau_ext        = tau_ext
                self.rho_scaling    = rho_scaling
            def eval(self, value, x):
                T = self.Tt0 - self.dT * (self.time*1.0/self.tau_ext * fenics.conditional(fenics.ge(self.time,0.0),1.0,0.0) - (self.time - self.tau_ext)*1.0/self.tau_ext * fenics.conditional(fenics.ge(self.time-self.tau_ext,0.0),1.0,0.0)  )
                rho = equilibrium_vapor_density(T)
                value[0] = rho/self.rho_scaling

        ### instanciate T boundary expressions
        # in case custom expressions are needed, instanciate them as such. make sure to update e.g. T_top.time += dt or some such at solution time.
        T_top = TBoundaryExpression(Tt0 = T_top_0_p, dT = dTemp_p,time=18000.0,tau_ext=t_tau,element=self.W_T.ufl_element())
        T_bottom = fenics.Expression('T_bottom_0', T_bottom_0=T_bottom_0_p,element=self.W_T.ufl_element())

        # boundary conditions are defined as specific obejcts. They require the function space of the trial function, the value or expression of the boundary condition, and the boundary.  
        ## apply Temp BC
        bc_T_top = fenics.DirichletBC(self.W_T, T_top, top)
        bc_T_bottom = fenics.DirichletBC(self.W_T, T_bottom, bottom)

        ## rho boundary conditions
        rho_top = rhoBoundaryExpression(Tt0 = T_top_0_p, dT = dTemp_p,time=18000.0,tau_ext=t_tau,rho_scaling = 1.0,element=self.W_r.ufl_element())
        rho_bottom = rhoBoundaryExpression(Tt0 = T_top_0_p, dT = dTemp_p,time=0.0,tau_ext=t_tau,rho_scaling = 1.0,element=self.W_r.ufl_element())

        # apply values
        bc_rho_top = fenics.DirichletBC(self.W_r, rho_top, top)
        bc_rho_bottom = fenics.DirichletBC(self.W_r, rho_bottom, bottom)

        # vn boundary
        vn_top = fenics.Expression('vn_top', vn_top=0.0,element=self.W_vn.ufl_element())
        vn_bottom = fenics.Expression('vn_bottom', vn_bottom=0.0,element=self.W_vn.ufl_element())

        bc_vn_top = fenics.DirichletBC(self.W_vn, vn_top, top)
        bc_vn_bottom = fenics.DirichletBC(self.W_vn, vn_bottom, bottom)

        # concatenate BCs
        # for the coulped pde's, the boundary conditions have to be joined.
        self.bcs_mixed = [bc_rho_top, bc_rho_bottom, bc_T_top, bc_T_bottom]     # without vn bcs
        # self.bcs_mixed = [bc_vn_top, bc_vn_bottom, bc_rho_top, bc_rho_bottom, bc_T_top, bc_T_bottom] # with vn bcs

    #===============================================================================
    # # Initial conditions #########################################################
    #===============================================================================
    def set_initial_conditions(self,):
        """
        defines initial_condition_expressions. These have to follow a specific format and are included in the variational problem later.
        # initial conditions may be time-dependent, then .time needs to be updated at solution time.
        """
        # set boundary values
        T_top_0_p       = temperature_om    # base top temperature in K
        T_bottom_0_p    = temperature_om    # base bottom temperature in K
        dTemp_p         = 20.0              # temperature drop at top in K
        
        #### InitialConditionExpressions can be defined arbitrarily and can depend on spacial coordinates (x, scalar or vector)
        class InitialConditionExpression(fenics.Expression):
            def __init__(self,Tt0_in,dT_in,rho_scaling_in, T_scaling_in, H_in, **kwargs):
                self.Tt0_in            = Tt0_in
                self.dTemp_in          = dT_in
                self.rho_scaling_in    = rho_scaling_in
                self.T_scaling_in      = T_scaling_in
                self.H_in              = H_in

            def eval(self, value, x):
                # Define Temperature
                T = self.Tt0_in - self.dTemp_in*x[0]/self.H_in
                # define vn
                value[0] = 0.0
                # Define vapor density
                rho = equilibrium_vapor_density(T)
                value[1] = rho/self.rho_scaling_in
                # set set Temperature
                value[2] = T/self.T_scaling_in
            def value_shape(self):
                return (3,)

        combined_D = InitialConditionExpression(Tt0_in = T_bottom_0_p,dT_in= dTemp_p,rho_scaling_in=1.0,T_scaling_in = 1.0,H_in=self.H,element=self.W_mixed.ufl_element())
        # the expressions are 'interpolated' onto the mesh to get nodal initial values
        self.w_mixed_n = fenics.interpolate(combined_D,self.W_mixed)

        class phiIceExpression(fenics.Expression):
            def __init__(self,H_p,**kwargs) -> None:
                self.H_p = H_p
            def eval(self, value, x):
                # gauss curve
                sigma_sqrd = 0.0000005
                mu         = 0.01
                offset     = 0.3
                corr       = 0.00177
                pi         = 3.14159265359
                value[0]   = offset + 0.2*corr/(fenics.sqrt(2*pi*sigma_sqrd))*fenics.exp(-((x[0]*self.H_p - mu)**2)/(2*sigma_sqrd))
            def value_shape(self):
                return (1,)
        phi_ex = phiIceExpression(H_p = self.H_p,element=self.W_phi.ufl_element())
        self.phi_i_n = fenics.interpolate(phi_ex,self.W_phi)

    #===============================================================================
    # # Define variational problem #################################################
    #===============================================================================
    def setup_variational_problem(self):
        """
        implements the variational formulation of the pde problem. 
        generally: _n suffix indicates previous time-step solution. T -> current Temperature. T_n -> previous time-step solution. approx dT/dt = (T-Tn)/dt
        1) define parameters
        2) prepare solution variables
        3) set equations
        4) join equation w/ boundary conditions to problem/solver
        """
        # get necessary fenics components for integration
        dx, grad, inner = fenics.dx, fenics.grad, fenics.inner
        # dx, dS, dot, grad, inner, div = fenics.dx, fenics.dS, fenics.dot, fenics.grad, fenics.inner, fenics.div

        ### 1) define paramters
        # wrap constant parameters as fenics.Constant()
        beta_const = fenics.Constant(5.5e5)         # []
        rho_i_const = fenics.Constant(917)          # [kg/m3]
        s_const = fenics.Constant(3770)             # [m2/m3]
        L_const = fenics.Constant(2.6e9)            # [J/kg]
        
        def Deff(phi):
            """ 
            returns effective vapor diffusion coefficient D_eff in [m2/s]
            """
            return effective_diffusion_constant(phi)

        def keff(phi):
            """
            returns effective thermal conductivity coefficient k_eff in [W/m/K]
            """
            return effective_thermal_conductivity(phi)

        def rhoCp(ice_volume_fraction):
            """
            returns heat capacity in [J/m3/K]
            """
            return rhoCp_eff(ice_volume_fraction)

        #### 2) prepare solution variables
        # Set time integration scheme: 0: Euler Forward, 0.5: Crank-Nicolson, 1: Euler Backward, 2/3: Galerkin
        theta = fenics.Constant(0.5)
        ## redefine solution functions on solution space
        # define variables for phi_i
        self.phi_i = fenics.Function(self.W_phi)
        # define variables for coupled equation: vn, rho, T
        self.w_mixed = fenics.Function(self.W_mixed)    
        self.vn, self.rho, self.T = fenics.split(self.w_mixed)         # current time step
        self.vn_n, self.rho_n, self.T_n = fenics.split(self.w_mixed_n) # _n: previous time step solution. Defined on the same solution space

        ### 3) set equations / Calonne Equations ######################
        # ice equation
        F_ice = self.psi_phi*(self.phi_i - self.phi_i_n)*dx             # LHS
        F_ice -= theta*self.psi_phi*s_const*self.vn*self.dt*dx          # source backward
        F_ice -= (1-theta)*self.psi_phi*s_const*self.vn_n*self.dt*dx    # source forward

        # vn equation
        F_vn =  self.psi_vn*self.vn*dx 
        F_vn -= self.psi_vn/beta_const * (self.rho - equilibrium_vapor_density(self.T) )/equilibrium_vapor_density(self.T)*dx
        # vapor equation
        F_vap = self.psi_rho*(self.rho - self.rho_n)*dx                                                                                 # LHS / time derivative (dt and coefficients moved to the other side)
        F_vap += theta*Deff(self.phi_i_n)/(1-self.phi_i_n)/(self.H_p**2)*inner(grad(self.psi_rho), grad(self.rho))*self.dt*dx           # spatial derivative term forward
        F_vap += (1-theta)*Deff(self.phi_i_n)/(1-self.phi_i_n)/(self.H_p**2)*inner(grad(self.psi_rho), grad(self.rho_n))*self.dt*dx     # spatial derivative term backward
        F_vap += theta*self.psi_rho*rho_i_const/(1-self.phi_i_n)*s_const*self.vn*self.dt*dx                                             # source term forward
        F_vap +=(1-theta)*self.psi_rho*rho_i_const/(1-self.phi_i_n)*s_const*self.vn_n*self.dt*dx                                        # source term backward
        # energy equation
        F_en = self.psi_T*(self.T-self.T_n)*dx                                                                                          # LHS / time derivative (dt and coefficients moved to the other side)
        F_en += theta*keff(self.phi_i_n)/rhoCp(self.phi_i_n)/(self.H_p**2)*inner(grad(self.psi_T),grad(self.T))*self.dt*dx              # spatial derivative term forward
        F_en += (1-theta)*keff(self.phi_i_n)/rhoCp(self.phi_i_n)/(self.H_p**2)*inner(grad(self.psi_T),grad(self.T_n))*self.dt*dx        # spatial derivative term backward
        F_en -= theta*self.psi_T*L_const/rhoCp(self.phi_i_n)*s_const*self.vn*self.dt*dx                                                 # source term forward
        F_en -= (1-theta)*self.psi_T*L_const/rhoCp(self.phi_i_n)*s_const*self.vn_n*self.dt*dx                                           # source term backward

        ### 4) join equation w/ boundary conditions to problem/solver ######################
        # fenics splits the FEM in equation, derivative, problem and solver classes. The equations are defined above.
        # the derivatives can be defined straight-forwardly with the equation and the trial-function to derive to.
        # the problems join the equation, trial function, derivatives and boundary conditions in one object.
        # the solver class wraps around the problem.

        # ice 
        JF_ice = fenics.derivative(F_ice, self.phi_i, fenics.TrialFunction(self.W_phi))
        problem_ice = fenics.NonlinearVariationalProblem(F_ice, self.phi_i, J=JF_ice)
        self.solver_ice = fenics.NonlinearVariationalSolver(problem_ice)

        # coupled vn, vapor, heat
        F_mixed = (F_vn + F_vap + F_en)
        JF_mixed = fenics.derivative(F_mixed, self.w_mixed, fenics.TrialFunction(self.W_mixed))
        problem_mixed = fenics.NonlinearVariationalProblem(F_mixed, self.w_mixed, bcs = self.bcs_mixed,J=JF_mixed)
        self.solver_mixed = fenics.NonlinearVariationalSolver(problem_mixed)

        
    #===============================================================================
    ## Solution settings
    #===============================================================================
    def setup_solver(self):
        prm = self.solver_mixed.parameters
        prm['newton_solver']['absolute_tolerance'] = 1E-10
        prm['newton_solver']['relative_tolerance'] = 1E-11
        prm['newton_solver']['convergence_criterion']   = 'residual'
        prm['newton_solver']['maximum_iterations'] = 20000
        prm['newton_solver']['error_on_nonconvergence']   = False
        prm['newton_solver']["linear_solver"] = "lu"
        prm['newton_solver']["krylov_solver"]["absolute_tolerance"] = 1E-12
        prm['newton_solver']["krylov_solver"]["relative_tolerance"] = 1E-13
        prm['newton_solver']["krylov_solver"]["maximum_iterations"] = 5000

        self.solver_ice.parameters = prm

#===============================================================================
# Time-stepping ################################################################
#===============================================================================
    def simulate(self,path):
        """
        runs a simulation with the problem defined before.
        1) define the output files, drop the initial condition there.
        2) set best guess for first solution
        3) setup timestep iterations
        4) iterate over timesteps
        """
        print("Start simulations")
        #setup directory
        path_root = Path(path).joinpath('data','ne_{}_dt_{}'.format(self.ne,self.dt))
        path_root.mkdir(exist_ok=True,parents=True)
        ###  1) define output files
        print("1) initialize output files.")
        ## drop mesh
        fnameM = str(path_root.joinpath('mesh.pvd').absolute())
        fileM = fenics.File(fnameM)
        # write file
        fileM << self.mesh

        ## output for mixed solution: vn, rho, T
        fname_mixed = str(path_root.joinpath('data_mixed_.pvd').absolute())
        file_mixed = fenics.File(fname_mixed)
        # get another variable on the same function space, to disentangle the output from the solution
        output_mixed = fenics.Function(self.W_mixed)
        # assing acutal value to output variables
        output_mixed.assign(self.w_mixed_n)
        # write file
        file_mixed << output_mixed

        ## output for phi
        fname_phi = str(path_root.joinpath('data_phi_.pvd').absolute())
        file_phi = fenics.File(fname_phi)
        output_phi = fenics.Function(self.W_phi)
        output_phi.assign(self.phi_i_n)
        file_phi << output_phi

        ###  2) set first solution to initial solution (closeness makes finding a good solution easier)
        print("2) set best guess of first solution.")
        self.phi_i.assign(self.phi_i_n)
        self.w_mixed.assign(self.w_mixed_n)

        ###  3) setup timestep iterations
        print("3) setup time iterations.")
        # Start time integration
        t0 = cputime.clock()
        solution_error = 0
        time = 0
        its_counter = 0

        print("4) start iteration over time-steps")
        # compute number of time steps
        steps = math.ceil(self.t_end / self.dt) #integer value at least as large as t_end/dt
        # iterate over time steps 
        for idx in range(steps):
            # Update current time
            time += self.dt

            # print progress
            if self.verbosity > 0:
                progress = (idx / steps*100) # progress in % 
                self.print_progress(progress=progress,time=time,interval=5)

            ## if BCs are time-dependent, update BC times here.
            # T_top.time = time
            # rho_top.time = time

            ## Compute solution for coupled vn,rho,T equations (mixed)
            if self.verbosity>5:
                print("#### solving physical problem for t=",time," with stepsize ", self.dt)
            (newton_its, solved_bool) = self.solver_mixed.solve()
            
            ## with the new solution of vn, rho, T -> solve ice equation
            (newton_its, solved_bool) = self.solver_ice.solve()

            # if dt_output: drop solution
            if time%self.dt_out==0:
                output_mixed.assign(self.w_mixed)
                file_mixed << output_mixed

                output_phi.assign(self.phi_i)
                file_phi << output_phi

            # assing new solution to previous timestep solution
            self.w_mixed_n.assign(self.w_mixed)
            self.phi_i_n.assign(self.phi_i)

            # update iteration counter
            its_counter = its_counter + 1


        t1 = cputime.clock()

        if solution_error == 0:
            print("#### simulation successful!")
            print("solved in ", its_counter, " iterations and ", t1-t0, "seconds")
        else :
            print("simulation broke down with error code ", solution_error)

    def print_progress(self,progress,time,interval=5):
        if progress % interval == 0:
            # set current time
            self.t_curr = cputime.clock()
            # if previous time exists: compute time difference and runtime estimation
            if hasattr(self, 't_last'):
            # if self.t_last is not None:
                t_diff = (self.t_curr-self.t_last)/60
                t_total = t_diff*100/interval  # estimated total runtime in minutes
                t_remaining = t_diff*(100-progress)/interval  # estimated remaining runtime in minutes

            else:
                t_diff = 0.0 
                t_total = -999.9
                t_remaining = -999.9

            output_str = 'simulation progress: ' + '{0:.0f}'.format(progress) + '% - simulation time ' + '{0:.2f}'.format(time/60) +'m / ' + '{0:.2f}'.format(self.t_end/60)
            output_str += 'm -- last step: ' + '{0:.2f}'.format(t_diff) + 'm -- estimated runtime remaining/total: ' + '{0:.2f}'.format(t_remaining) + 'm / ' + '{0:.2f}'.format(t_total) + 'm '
            print(output_str)
            # update last time
            self.t_last = self.t_curr 
        else:
            return

# ### single run
# if __name__ == "__main__":
#     # instanciate 
#     problem = CalonneIceVaporHeat(ne=1000,verbosity=6)
#     path = '.'
#     problem.simulate(path)

# iteration over mesh resolutions and timesteps, to reproduce figure
if __name__ == "__main__":
    path = '.'
    # instanciate 
    ne_list = [10000,3500,3500,1000,350,100]
    dt_list = [60,60,2,60,60,60]
    for (nedx,dtdx) in zip(ne_list,dt_list):
        print('start simulation with {} elements and dt {}'.format(nedx,dtdx))
        problem = CalonneIceVaporHeat(ne=nedx,dt=dtdx,verbosity=3)
        problem.simulate(path)

