## this function imports data from vtk files and plots with matplotlib

################################################################################
# import #####
################################################################################
import matplotlib.pyplot as plt

from pathlib import Path

import numpy as np
import vtk
from vtk.util.numpy_support import vtk_to_numpy
from vtk.numpy_interface import dataset_adapter as dsa
from matplotlib.ticker import FormatStrFormatter

################################################################################
# define import function #####
################################################################################
def getNodeArrays(vtu_filename):
    # read file
    print('Reading ' + vtu_filename + '...')
    reader = vtk.vtkXMLUnstructuredGridReader()
    reader.SetFileName(vtu_filename)
    reader.Update()
    data = reader.GetOutput()

    print('Number of points: ' + str(reader.GetNumberOfPoints()))
    print('Number of cells: ' + str(reader.GetNumberOfCells()))

    print('Number of point arrays: ' + str(reader.GetNumberOfPointArrays()))
    print('Number of cell arrays: ' + str(reader.GetNumberOfCellArrays()))

    for i in range(reader.GetNumberOfPointArrays()):
        print('Name of point array ' + str(i) + ': ' + str(reader.GetPointArrayName(i)))

    print('Returning numpy arrays for point data from point array 0')
    # get arrays
    points = data.GetPoints()
    x = vtk_to_numpy(points.GetData())[:,0]

    usg = dsa.WrapDataObject( data )
    y = usg.PointData[reader.GetPointArrayName(0)]
    print('shape of x:',x.shape)
    print('shape of y:',y.shape)
    print('... done.\n')


    return x, y

################################################################################
# import data #####
################################################################################
t = [0,30,60,90,180,360,720]
dt_out = 600
for i in range(1,len(t)):
     t[i] = int(t[i]*dt_out/3600)

## Calonne

def plot_figure(config_list):

    # make figure
    ################################################################################
    # prepare plots #####
    ################################################################################
    # global figure pimping
    #plt.rc('text', usetex=True)
    plt.rc('xtick', labelsize=16)
    plt.rc('ytick', labelsize=16)
    plt.rc('axes', labelsize=10)
    plt.rc('legend', fontsize=12)

    ################################################################################
    # plots #####
    ################################################################################
    #CONSTANTS
    NPOINTS = 12
    COLOR='blue'
    RESFACT=10
    # MAP='seismic'
    MAP='plasma'
    MAP2=MAP #'spring'
    MAP3=MAP # 'summer'
    cm = plt.get_cmap(MAP)
    cm2 = plt.get_cmap(MAP2)
    cm3 = plt.get_cmap(MAP3)

    fig, axes = plt.subplots(3,1, figsize=(10,15))
    xmin = 0.0
    xmax = 0.02
    ymin = 0.29
    ymax = 0.51
    ymin2 = -0.0000025
    ymax2 = 0.0000025
    # t1
    #t2
    #### call plot function
    for idx,config in enumerate(config_list):
        fig, axes = load_data_and_plot_figure(fig=fig, axes=axes ,path=config['path'], ne=config['ne'], dt=config['dt'], idx=idx)


    # pimp style
    ax=axes[0]
    ax.set_ylabel(r'$\phi_i$ (-)',fontsize=20)
    ax.set_xlabel(r'$z$ (m)',fontsize=20)
    ax.legend(loc='best',fontsize = 16)
    ax.set_title('a',fontsize = 16)
    ax.set_xlim([0.0,0.02])
    # ax.set_ylim([-0.0000045,0.0000015])
    ax.tick_params(labelsize=16)    # fontsize of the tick labels
    ax.grid(True)
    ax=axes[1]
    ax.set_ylabel(r'$\phi_i$ (-)',fontsize=20)
    ax.set_xlabel(r'$z$ (m)',fontsize=20)
    ax.legend(loc='best',fontsize = 16)
    ax.set_title('b',fontsize = 16)
    ax.set_xlim([-0.0001,0.0006])
    ax.set_ylim([0.38,0.405])
    ax.tick_params(labelsize=16)    # fontsize of the tick labels
    ax.grid(True)

    ax=axes[2]
    ax.set_ylabel(r'$\phi_i$ (-)',fontsize=20)
    ax.set_xlabel(r'$z$ (m)',fontsize=20)
    ax.legend(loc='lower right',fontsize = 16)
    ax.set_title('c',fontsize = 16)
    ax.set_xlim([0.008,0.011])
    ax.set_ylim([0.27,0.45])
    ax.tick_params(labelsize=16)    # fontsize of the tick labels
    ax.grid(True)

    # save figure
    fig.tight_layout()
    # ax.legend(loc='best',fontsize = 16)


    fig.savefig('meshres_timestep.png')


def load_data_and_plot_figure(fig, axes ,path, ne, dt, idx):
    # load data
    # phi
    path_root = Path(path)
    x1,y1 = getNodeArrays(str(path_root.joinpath('data_phi_000000.vtu').absolute()))
    x2,y2 = getNodeArrays(str(path_root.joinpath('data_phi_000030.vtu').absolute()))
    x3,y3 = getNodeArrays(str(path_root.joinpath('data_phi_000060.vtu').absolute()))
    x4,y4 = getNodeArrays(str(path_root.joinpath('data_phi_000090.vtu').absolute()))
    x5,y5 = getNodeArrays(str(path_root.joinpath('data_phi_000180.vtu').absolute()))
    x6,y6 = getNodeArrays(str(path_root.joinpath('data_phi_000360.vtu').absolute()))
    x7,y7 = getNodeArrays(str(path_root.joinpath('data_phi_000720.vtu').absolute()))
    # x7,y7 = getNodeArrays(str(path_root.joinpath('data_phi_007200.vtu').absolute()))

    # print('x1.shape {}'.format(x1.shape))
    # print('y1.shape {}'.format(y1.shape))

    label = 'ne={}; dt={}'.format(ne,dt)
    color_list = ['steelblue', 'b', 'g', 'darkorange', 'r', 'm']
    marker_list = ['x', 'o', '.', "v", '+','s']
    markersize_list = [6, 8, 5, 8, 8, 8]
    alpha_list = [0.9,0.95,0.9,0.8,0.7,0.6]
    marker = marker_list[idx]
    markersize=markersize_list[idx]
    alpha=alpha_list[idx]
    color = color_list[idx]

    ax = axes[0]
    ax.plot(x7,y7, label=label, color=color,linewidth=1.5,alpha=alpha,linestyle='-', marker=marker,markersize=markersize)
    
    # lower resolution doesn't fit in that plot -> filter
    if ne>350:
        ax = axes[1]
        ax.plot(x7,y7, label=label, color=color,linewidth=1.5,alpha=alpha,linestyle='-', marker=marker,markersize=markersize)
        # ax.plot(x7,y7, 's-', label=r'ne=10000; dt=60', color='steelblue',linewidth=1.5, markersize=6,alpha=0.9)

    ax = axes[2]
    ax.plot(x7,y7, label=label, color=color,linewidth=1.5,alpha=alpha,linestyle='-', marker=marker,markersize=markersize)
    
    return fig, axes



## iteration over mesh
if __name__ == "__main__":
    path_root = Path('.')
    config_list = []
    # instanciate 
    ne_list = [10000,3500,3500,1000,350,100]
    dt_list = [60,60,2,60,60,60]
    for (nedx,dtdx) in zip(ne_list,dt_list):
        config = {
            'path': path_root.joinpath('data','ne_{}_dt_{}'.format(nedx,dtdx)),
            'ne': nedx,
            'dt': dtdx,
        }
        config_list.append(config)
    plot_figure(config_list)
 
